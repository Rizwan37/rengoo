import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HttpModule } from "@angular/http";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { ValidateService } from "./services/validate.service";
import { MapService } from "./services/map.service";
import { EnergySoldService } from "./services/energySold.service";
import { EnergyConsumptionService } from "./services/energyconsumption.service";
import { AuthService } from "./services/auth.service";
import { DataService } from "./services/data.service";
import { FlashMessagesModule } from "angular2-flash-messages";
import { AuthGuard } from "./guards/auth.guard";
import { AdminGuard } from "./guards/admin.guard";
import { DataTablesModule } from "angular-datatables";

import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";
import { NgbModule, NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { AppComponent } from "./app.component";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { LoginComponent } from "./components/login/login.component";
import { HomeComponent } from "./components/home/home.component";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { ProfileComponent } from "./components/profile/profile.component";
import { RegisterComponent } from "./components/register/register.component";
import { UserModelComponent } from "./components/user-model/user-model.component";
import { VerifyComponent } from "./components/verify/verify.component";
import { EnergyProductionComponent } from "./components/energy-production/energy-production.component";

import { ForgetComponent } from "./components/forget/forget.component";
import { ResetComponent } from "./components/reset/reset.component";
import { UserpanelComponent } from "./components/userpanel/userpanel.component";
import { UserPanelNavbarComponent } from "./components/user-panel-navbar/user-panel-navbar.component";
import { MapComponent } from "./components/googlemap/map.component";
import { EnergySoldComponent } from "./components/EnergySoldMap/energysold.component";
import { EnergyConsumptionComponent } from "./components/EnergyConsumptionMap/energyconsumption.component";
import { AgmCoreModule } from "@agm/core";
import { CurrentConsumptionComponent } from "./components/user-panel-navbar/user-current-consumption/consumption.component";
import { UserPowerStorageComponent } from "./components/user-panel-navbar/user-power-storage/user-power-storage.component";
import { UserTradeComponent } from "./components/user-panel-navbar/user-trade/user-trade.component";
import { UserEnergyMixComponent } from "./components/user-panel-navbar/user-energy-mix/user-energy-mix.component";
import { UserProfileComponent } from "./components/user-panel-navbar/user-profile/user-profile.component";
import { EditProfileComponent } from "./components/edit-profile/edit-profile.component";
import { MixenergyproduceComponent } from "./components/mixenergyproduce/mixenergyproduce.component";
import { UserBillingComponent } from './components/user-billing/user-billing.component';
import { BuyerOffersComponent } from "./components/user-panel-navbar/buyer-offers/buyer-offers.component";
import { WatsonComponent } from './components/watson/watson.component';
import { NetworkBillingComponent } from './components/network-billing/network-billing.component';
import {DataTableModule} from "angular-6-datatable";


const appRoutes: Routes = [
  { path: "", component: LoginComponent },
  { path: "watson", component: WatsonComponent },
  { path: "register", component: RegisterComponent },
  { path: "login", component: LoginComponent },
  { path: "forget", component: ForgetComponent },
  { path: "verify/:token", component: VerifyComponent },
  { path: "reset/:token", component: ResetComponent },
  {
    path: "dashboard",
    component: DashboardComponent,
    canActivate: [AdminGuard]
  },
  {
    path: "profile",
    component: NavbarComponent,
    canActivate: [AdminGuard],
    children: [
      { path: "", component: ProfileComponent, canActivate: [AdminGuard] },
      { path: "map", component: MapComponent, canActivate: [AdminGuard] },
      { path: "networkBilling", component: NetworkBillingComponent, canActivate: [AdminGuard] }
    ]
  },
  {
    path: "userPanel",
    component: UserpanelComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: "consumption",
        component: CurrentConsumptionComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "mix",
        component: UserEnergyMixComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "mixenergyproduce",
        component: MixenergyproduceComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "storage",
        component: UserPowerStorageComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "trade",
        component: UserTradeComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "userProfile",
        component: UserProfileComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "edit",
        component: EditProfileComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "sold",
        component: EnergySoldComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "consump",
        component: EnergyConsumptionComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "energyproduction",
        component: EnergyProductionComponent,
        canActivate: [AuthGuard]
      },
      {

        path: "userbilling",
        component: UserBillingComponent,
      },{
        path: "buyerOffers",
        component: BuyerOffersComponent,
        canActivate: [AuthGuard]
      }
    ]
  }

  //  {path:'consump', component: EnergyConsumptionComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    HomeComponent,
    DashboardComponent,
    ProfileComponent,
    RegisterComponent,
    UserModelComponent,
    VerifyComponent,
    ForgetComponent,
    ResetComponent,
    UserpanelComponent,
    UserPanelNavbarComponent,
    UserEnergyMixComponent,
    UserPowerStorageComponent,
    UserTradeComponent,
    MapComponent,
    UserProfileComponent,
    EnergySoldComponent,
    CurrentConsumptionComponent,
    EnergyConsumptionComponent,
    EditProfileComponent,
    EnergyProductionComponent,
    MixenergyproduceComponent,
    UserBillingComponent,
    BuyerOffersComponent,
    WatsonComponent,
    NetworkBillingComponent

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    DataTableModule,
    HttpModule,
    ReactiveFormsModule,
    FormsModule,
    DataTablesModule,
    RouterModule.forRoot(appRoutes),
    FlashMessagesModule.forRoot(),

    NgbModule.forRoot(),
    Ng4LoadingSpinnerModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyBrfeH8C0hIv6ZfD93KzaeuAz0aroFt2Y0",
      libraries: ['geometry']
    }),
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    ValidateService,
    AuthService,
    EnergyConsumptionService,
    DataService,
    MapService,
    EnergySoldService,
    AuthGuard,
    NgbActiveModal,
    AdminGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
