import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable, observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  BaseUrl: any = "https://hyperledger-power-watson-rest-noisy-rabbit.eu-gb.mybluemix.net";

  RengooUrl: any = this.BaseUrl + '/api/User';
  MixUrl: any = this.BaseUrl + '/api/User';
  ConsumptionUrl = this.BaseUrl + '/api/queries/GetConsumptionForTable?user=resource%3Awaltson.poc.hyperledger.User%23';

  PowerStorageUrl: any = this.BaseUrl + '/api/User';

  PowerStorageUrlById: any = this.BaseUrl + '/api/User/';
  BuyEnergyUrl: any = this.BaseUrl + '/api/buyEnergy';
  BuyersOpenToOfferUrl: any = this.BaseUrl + '/api/queries/GetAllOpenOfferedUsers';
  GetAllProducersUrl: any = this.BaseUrl + '/api/queries/GetAllProducer';
  GetAllProsumersUrl: any = this.BaseUrl + '/api/queries/GetAllProsumer';
  GetAllConsumersUrl: any = this.BaseUrl + '/api/queries/GetAllConsumers';
  productionUrl: any = this.BaseUrl + '/api/queries/GetProductionsForTable?user=resource%3Awaltson.poc.hyperledger.User%23';
  getOpenUsersUrl: any = this.BaseUrl + '/api/queries/GetAllOpenOfferedUsers';
  GetAllProducersNetwork: any = this.BaseUrl + '/api/User';
  UpdateBalanceURL: any = this.BaseUrl + '/api/AddCoinBalance';
  GetUserById: any = this.BaseUrl + '/api/User';
  GetAllUsers: any = this.BaseUrl + '/api/User';

  SendOfferURL: any = this.BaseUrl + "/api/buyEnergy";
  constructor(private http: HttpClient) {

  }

  getAllUsers(): Observable<HttpResponse<any>> {
    return this.http.get<any>(this.GetUserById);
  }

  getUserById(userId: any): Observable<HttpResponse<any>> {
    return this.http.get<any>(this.GetUserById + "/" + userId);
  }

  getAllProducers(): Observable<HttpResponse<any>> {

    return this.http.get<any>(this.GetAllProducersNetwork);
  }

  getAllProsumers(): Observable<HttpResponse<any>> {

    return this.http.get<any>(this.GetAllProsumersUrl);
  }

  getAllConsumers(): Observable<HttpResponse<any>> {

    return this.http.get<any>(this.GetAllConsumersUrl);
  }

  getAllOpenOffers(): Observable<HttpResponse<OpenOffers[]>> {
    let httpHeaders = new HttpHeaders();
    httpHeaders.append('Content-Type', 'application/json');
    return this.http.get<OpenOffers[]>(this.getOpenUsersUrl,
      {
        headers: httpHeaders,
        observe: 'response'
      }
    );
  }




  getUsers() {

  }

  sendOffer(data: any): Observable<HttpResponse<any>> {
    return this.http.post<any>(this.SendOfferURL, data, {
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    })
  }

  createUser(newUser) {


    return this.http.post(this.RengooUrl, newUser, {
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    }).pipe(map(res => res));
  }



  getConsumption(userId: any) {

    return this.http.get(this.ConsumptionUrl + userId, {
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    }).pipe(map(res => res));
  }

  updateTokenBalance(dataObject: any) {
    return this.http.post<any>(this.UpdateBalanceURL, dataObject,
      {
        headers: new HttpHeaders().append('Content-Type', 'application/json')
      });
  }


  getMix(userId: any): Observable<HttpResponse<any>> {
    // console.log('User id detail is ', userId);
    // console.log('USER ID IS ', this.MixUrl+'/'+userid)
    return this.http.get<any>(this.MixUrl + '/' + userId);
    // return this.http.get(this.MixUrl+"5bc982a30aa86f0eb4b282b4").pipe(map(res =>res));
  }

  getBuyersOpenToOffer() {
    return this.http.get(this.BuyersOpenToOfferUrl, {
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    }).pipe(map(res => res));
  }


  getPowerStorateData(): Observable<HttpResponse<any>> {

    return this.http.get<any>(this.PowerStorageUrl, {
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    });
  }

  getPowerStorageDataByUserId(id: any) {
    return this.http.get(this.PowerStorageUrlById + id, {
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    }).pipe(map(res => res));
  }

  buyEnergy(info) {

    return this.http.post(this.BuyEnergyUrl, info, {
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    }).pipe(map(res => res));
  }

  getEnergyProduction(userId: any) {


    return this.http.get(this.productionUrl + userId, {
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    }).pipe(map(res => res));
  }











}

export class OpenOffers {
  userID;
  name;
  distanceFromGrid;
  totalSale;
  pricePerKWH;
  energyProductionDetails;
  energyConsumption;
  energySold;
  totalEnergyConsumed;
  totalBoughtEnergy;
  totalSoldEnergy;
  balanceAmount;
  openOffers;
  location;
  userType;
  totalAvailableProduction;
  totalProduction;
  powerStorageRequirement;
  productionCapacity;
  batteryCapacity;
  openOffersList;
  longitude;
  latitude;
}