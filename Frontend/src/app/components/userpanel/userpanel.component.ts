import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { EnergySoldService } from '../../services/energySold.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';

declare var $: any;
@Component({
  selector: 'app-userpanel',
  templateUrl: './userpanel.component.html',
  styleUrls: ['./userpanel.component.css']
})
export class UserpanelComponent implements OnInit {



  Rangoo: String = 'https://hyperledger-power-watson-rest-noisy-rabbit.eu-gb.mybluemix.net/';

  sellForm: FormGroup;
  sellModel: any = {
    user: '',
    energyResource: '',
    productionCapacity: 0,
    production: 0
  }
  productionEnergyResources = [];

  constructor(private dataService: DataService, private formBuilder: FormBuilder, private authService: AuthService, private energySellService: EnergySoldService, private spinnerService: Ng4LoadingSpinnerService, private toastr: ToastrService, private router: Router) {

  }

  ngOnInit() {
    this.sellForm = this.formBuilder.group({
      energyResource: ['WIND_POWER', [
        Validators.required
      ]],
      productionCapacity: ['', [
        Validators.required,
        Validators.pattern('[+-]?([0-9]*[.])?[0-9]+'),
        this.ValidateFloatNumber
      ]]
    })


    this.dataService.getMix(JSON.parse(localStorage.getItem("user")).id).subscribe(res => {
      this.productionEnergyResources = res['energyProductionDetails'];
    });
  }

  sell() {
    this.spinnerService.show();
    this.authService.getProfile().subscribe(res => {
      this.sellModel.user = `waltson.poc.hyperledger.User#${res["user"]._id}`;
      this.sellModel.energyResource = this.sellForm.value.energyResource;
      this.sellModel.productionCapacity = parseInt(this.sellForm.value.productionCapacity);
      this.energySellService.sellEnergy(this.sellModel).subscribe(res => {
        this.toastr.success('Sucessfully Added!', 'Success');
        $("#sell-energy").modal("hide");
        this.spinnerService.hide();

        //this.ngOnInit();
        location.reload();

      }, err => {
        this.toastr.error('Error Occured! Please try again later.', 'Error');
        this.spinnerService.hide();
      })
    }, err => {
      this.toastr.error('Error Occured! Please try again later.', 'Error');
      this.spinnerService.hide();
    });

  }

  sendOffer() {
    this.spinnerService.show();

    this.authService.getProfile().subscribe(res => {
      this.sellModel.user = `waltson.poc.hyperledger.User#${res["user"]._id}`;
      this.sellModel.energyResource = this.sellForm.value.energyResource;
      let sendOffer = {
        seller: `waltson.poc.hyperledger.User#${res["user"]._id}`,
        energyResource: this.sellForm.value.energyResource,
        buyer: `waltson.poc.hyperledger.User#${localStorage.getItem("buyer_id")}`,
        grid: `waltson.poc.hyperledger.Grid#111`,
        purchaseUnitKWH: parseFloat(res["user"].PricePerKWH),
        approval: false
      }

      this.dataService.sendOffer(sendOffer).subscribe(res => {
        this.toastr.success("Offer send successfully!", "Success");
        this.spinnerService.hide();
      }, err => {
        this.toastr.error("You cannot send same enrgy resource offer twice", "Error");
        this.spinnerService.hide();
      });
    });
  }

  get productionCapacity() {
    return this.sellForm.get('productionCapacity');
  }

  sellToOpenOffers() {
    this.spinnerService.show();
    this.authService.getProfile().subscribe(res => {
      this.sellModel.user = `waltson.poc.hyperledger.User#${res["user"]._id}`;
      this.sellModel.energyResource = this.sellForm.value.energyResource;
      this.sellModel.productionCapacity = parseInt(this.sellForm.value.productionCapacity);
      this.energySellService.sellEnergy(this.sellModel).subscribe(res => {
        this.toastr.success('Sucessfully Added!', 'Success');
        this.spinnerService.hide();
      }, err => {
        this.toastr.error('Error Occured! Please try again later.', 'Error');
        this.spinnerService.hide();
      })
    }, err => {
      this.toastr.error('Error Occured! Please try again later.', 'Error');
      this.spinnerService.hide();
    });
  }

  ValidateFloatNumber(controls) {
    // Create a regular expression
    const Exp = /[+-]?([0-9]*[.])?[0-9]+/;
    // const Exp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    // Test email against regular expression
    if (Exp.test(controls.value)) {
      return null;
    } else {
      return { 'validateFloatNumber': true } // Return as invalid email
    }
  }

}
