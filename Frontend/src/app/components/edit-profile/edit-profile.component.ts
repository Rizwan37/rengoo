import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: [ './edit-profile.component.css' ]
})
export class EditProfileComponent implements OnInit {

  users:Object;
  users1:Object;
  username:any;
   user:any;
  userId:any;
  
  editForm: FormGroup
  sellerToggleValue : any =false;
  toggleValue : any =false;
  
  constructor(
                private authService: AuthService,
                private toastr: ToastrService,
                private spinnerService: Ng4LoadingSpinnerService,
                private formBuilder: FormBuilder,
                private router: Router
              ){

  }

  ngOnInit(): void {
   this.toggleValue= window.localStorage.getItem("power");
   this.sellerToggleValue= window.localStorage.getItem("available");
  
    

      this.authService.getProfile().subscribe(profile => {
          
          this.user = profile['user'];
          this.username =profile['user'].username;

          this.spinnerService.hide();
        },
         err => {
           this.spinnerService.hide();
           return false;
         });
  }

  async editFormValue() {
    
    if(this.toggleValue == null){
      this.toggleValue = false;
    }
    this.spinnerService.show();
    const users = {
      $class: "waltson.poc.hyperledger.PowerStorageRequirement",
      user:'waltson.poc.hyperledger.User#' + JSON.parse(localStorage.getItem("user")).id, // User id
      powerStorage: this.toggleValue
    }
  if(this.sellerToggleValue == null){
    this.sellerToggleValue = false;
  }

  const users1 = {
    "$class": "waltson.poc.hyperledger.ChangeOfferBuyer",
    user:'waltson.poc.hyperledger.User#' + JSON.parse(localStorage.getItem("user")).id, // User id
    available: this.sellerToggleValue
}

  await this.authService.powerStorage(users).subscribe(data => {
    
    if (data['powerStorage'] == true) {
      window.localStorage.setItem("power",data['powerStorage'])
      this.toastr.success("Power storage is on", "Success");//example
    } else {
      window.localStorage.setItem("power",data['powerStorage'])
      this.toastr.success("Power storage is off", "Success");//example
    }

    this.authService.openSeller(users1).subscribe(data => {
    
    if (data['available'] == true) {
      window.localStorage.setItem("available",data['available'])
      this.spinnerService.hide();
      this.toastr.success("Open Offer is on", "Success");//example
      this.router.navigate(['/userPanel/userProfile']);
    } else {
      window.localStorage.setItem("available",data['available'])
      this.spinnerService.hide();
      this.toastr.success("Open Offer is off", "Success");//example
      this.router.navigate(['/userPanel/userProfile']);
    }
      
      this.spinnerService.hide();
      // this.router.navigate(['/userPanel/userProfile']);
      location.href = `${location.origin}/userPanel/userProfile`;
      });
    });
  }
}
