import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: String;
  password: String;

  constructor(
    private authService:AuthService,
    private router:Router,
    private flashMessage:FlashMessagesService,
    private spinnerService: Ng4LoadingSpinnerService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
  }

  onLoginSubmit(){
    const user = {
      email: this.email,
      password: this.password
    }

this.spinnerService.show();
    this.authService.authenticateUser(user).subscribe(data => {

      if(data['success'] === 'notActivate'){
        this.toastr.error('Admin did not approved your account yet', "Error");
        this.flashMessage.show('Admin did not approved your account yet', {
          cssClass:'text-center alert-danger',
          timeout: 3000
        });
        this.spinnerService.hide();
        this.password = '';
        this.router.navigate(['login']);
      }

      if(data['success'] === true){
        this.toastr.success("Successfully logged in", "Success");
        this.authService.storeUserData(data['token'], data['user']);
        this.spinnerService.hide();
        if(data['user']['isAdmin'] === true){
          this.router.navigate(['profile/map']);
        }else{
          this.router.navigate(['userPanel/trade']);
        }

      } else if(data['success'] === false) {
        this.toastr.error("Error while login", "Error");
        this.spinnerService.hide();
        this.password = '';
        this.router.navigate(['login']);
      }
    }, err => {
      this.spinnerService.hide();
    });
  }

}
