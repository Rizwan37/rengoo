import { Component, OnInit ,AfterViewInit ,OnDestroy,ViewChild} from '@angular/core';
import { DataService } from '../../services/data.service';
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';
//import * as moment from 'moment';
import { Subject } from 'rxjs';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-user-billing',
  templateUrl: './user-billing.component.html',
  styleUrls: ['./user-billing.component.css']
})
export class UserBillingComponent implements  AfterViewInit, OnDestroy, OnInit{
  userPurchase = [];
  userSale = [];
  userType: any;
  energySoldLength: -1;
  sellerPrice: any;
  userSaleLength: any;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();



  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

 ourUserPrchase = []
  ourUserSale = []
  constructor(
    private dataService: DataService, private spinnerService: Ng4LoadingSpinnerService) { }


   ngOnInit() {
    

    this.dataService.getUserById(JSON.parse(localStorage.getItem("user")).id).subscribe(res => {


      this.userPurchase.push(res["energyConsumption"]);

      
     this.ourUserPrchase = this.userPurchase[0];
     
   
    
      
      
      

      this.ourUserPrchase = this.userPurchase[0];

      this.userType = res["userType"];
      this.userSaleLength = res["energySold"].length;
      this.userSale.push(res["energySold"]);
      this.ourUserSale = this.userSale[0];
      this.sellerPrice = res["pricePerKWH"]
      
      this.energySoldLength = res["energySold"].length;
      var __this = this;
      setTimeout(function(){__this.dtTrigger.next()}, 1);
    });

  

}

ngAfterViewInit(): void {
}


ngOnDestroy(): void {
  // Do not forget to unsubscribe the event
  this.dtTrigger.unsubscribe();
}
rerender(): void {

    this.dtTrigger.next();
 
}



  }
