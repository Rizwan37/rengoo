import { Component, OnInit, Input } from '@angular/core';
import { ValidateService } from '../../services/validate.service';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ToastrService } from 'ngx-toastr';
declare var $: any;

@Component({
  selector: 'app-user-model',
  templateUrl: './user-model.component.html',
  styleUrls: ['./user-model.component.css']
})
export class UserModelComponent implements OnInit {


  processing: boolean = false;
  user: any = {};
  username: string;
  activate: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private dataService: DataService,
    private router: Router,
    private spinnerService: Ng4LoadingSpinnerService,
    private toastr: ToastrService
  ) {

  }

  ngOnInit() {

  }

  openModal(id: any) {
    this.spinnerService.show();

    this.authService.getUserById(id).subscribe(res => {

      if (res['success'] === true) {

        this.user = res['message'];
        this.activate = this.user.activate;
        this.spinnerService.hide();

        $("#myModal").showAngularModal();

      }
    }, err => {
      this.spinnerService.hide();
    });
  }

  activateUser() {

    this.spinnerService.show();
    this.authService.getUserById(this.user._id).subscribe(res => {

      if (res['success'] === true) {

        let newUser = {
          "userID": this.user._id,
          "name": this.user.username,
          "distanceFromGrid": this.user.distanceFromGrid,
          "pricePerKWH": this.user.PricePerKWH,
          "balanceAmount": this.user.balanceAmount,
          "productionCapacity": this.user.productionCapacity,
          "latitude": this.user.Lattitude,
          "longitude": this.user.Longitude,
          "location": this.user.LocationDetail
        };


        this.dataService.createUser(newUser).subscribe(res => {


          this.authService.activateUser(this.user._id).subscribe(res => {

            if (res['success'] === true) {
              this.toastr.success("User account has been activated sucessfully", "Success");//example
              this.spinnerService.hide();
              $('#myModal').modal('hide');
            } else {

              this.toastr.error("Somthing went wrong please try again", "Error");//example
              this.spinnerService.hide();
            }
          })
        });

      }
    }, err => {
      this.spinnerService.hide();
    });
  }
}
