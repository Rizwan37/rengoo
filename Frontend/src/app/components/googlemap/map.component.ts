import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { GoogleMapsAPIWrapper, AgmMap, LatLngBounds, LatLngBoundsLiteral, AgmCoreModule, MapsAPILoader } from '@agm/core';

import { MapService } from '../../services/map.service';
import { DataService } from '../../services/data.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {EnergySoldService} from '../../services/energySold.service'

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NgZone } from '@angular/core';
declare var angular: any;
declare const google: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})


export class MapComponent implements OnInit, AfterViewInit {
 
  @ViewChild('AgmMap') agmMap: AgmMap;
  welcomeTxt: string = `Welcome to party!`;
  geoJsonObject: Object;

  distance: String;
  users$: Object;
  map = null;
  totalPro : any;
  totalConsumers = [];
  totalProsumers = [];
  totalProducers = [];
  totalProducersName = [];
  consumerTotalEngeryConsumed: any = 0;
  producerTotalEngeryConsumed: any = 0;
  prosumerTotalEngeryConsumed: any = 0;
  latitudedata = [];
  mapData = [];
  add: number = 0;
  dataLength = [];
  filterArray = [];
  dy : any = 90;
  totalProsumerInArea:any;
  totalConsumerInArea:any;
  totalProducerInArea : any;
  totlaconsumer : any;
  totlaproducer : any;
  totlaprosumer : any;
  totalConsumeEnergy : any;
  totalProsumeEnergy : any;



  theTexts: string;
  selectedOption: string;
  markers = [];

  allProsumers: any;
  constructor(
    private _demoService: MapService,
    private dataService: DataService,
    private spinnerService: Ng4LoadingSpinnerService,
    private _energysold: EnergySoldService

  ) { }
  getGeoJSON(): void {

    this._energysold.getBermuda().subscribe(datas => {
      this.geoJsonObject = datas;

    });
  }


  ngOnInit() {

    this.getGeoJSON();
    this.spinnerService.show();

    this._demoService.getFoods().subscribe(datas => {
      this.dataLength.push(datas);


      for (let i = 0; i < this.dataLength[0].length; i++) {
       
        if (this.dataLength[0][i].userType != "NONE") {
          this.filterArray.push(this.dataLength[0][i])
        }
      }
      
      this.latitudedata = this.arrayMap(this.filterArray, (loc, index) => {
        return {
          id: index, lat: loc.latitude,
          lng: loc.longitude, myName: loc.name,
          type: loc.userType,
          distance: loc.distanceFromGrid,
          locationname: loc.location,
          balanceamount: loc.balanceAmount,
          priceperkwh: loc.pricePerKWH
        }
      });

      this.getMarkersAndLoadMap(this.latitudedata);
      this.spinnerService.hide();
    });


    this.dataService.getAllConsumers().subscribe(res => {
      this.arrayMap(res, (loc, index) => {

        this.totalConsumers.push(res);
        this.consumerTotalEngeryConsumed += loc.totalEnergyConsumed;
      });
      
      window.localStorage.setItem("consumeenergy",this.consumerTotalEngeryConsumed)

      this.totalConsumerInArea = this.totalConsumers.length
      // store total consumer in local storage
      window.localStorage.setItem("consumer",this.totalConsumerInArea)
    }, err => {

    })

    this.dataService.getAllProsumers().subscribe(res => {
      this.arrayMap(res, (loc, index) => {
        this.totalProsumers.push(res);
        this.prosumerTotalEngeryConsumed += loc.totalProduction;
      });
      window.localStorage.setItem("prosumerenergy",this.prosumerTotalEngeryConsumed)

      this.totalProsumerInArea = this.totalProsumers.length
            // store total prosumer in local storage
      window.localStorage.setItem("prosumer",this.totalProsumerInArea)

    }, err => {
    })

    this.dataService.getAllProducers().subscribe(res => {
      this.arrayMap(res, (loc, index) => {
        if (loc.userType == "PRODUCER") {
          this.totalProducers.push(loc.energyProductionDetails);
          this.totalProducersName.push(loc.name);
        }
        
      });
      this.totalProducerInArea = this.totalProducers.length
            // store total producer in local storage
      window.localStorage.setItem("producer",this.totalProducerInArea)
    });

    
    angular.module('PubNubAngularApp', ["pubnub.angular.service"])
    .controller('MySpeechCtrl', function($rootScope, $scope, Pubnub) {
     
        $scope.totlaconsumer = window.localStorage.getItem("consumer");
        $scope.totlaprosumer = window.localStorage.getItem("prosumer");
        $scope.totlaproducer = window.localStorage.getItem("producer");
        
    });

  }
  
  speak(){
    this.totalConsumeEnergy=window.localStorage.getItem("consumeenergy")
    this.totalProsumeEnergy =window.localStorage.getItem("prosumerenergy")
    this.totlaconsumer = window.localStorage.getItem("consumer");
    this.totlaprosumer = window.localStorage.getItem("prosumer");
    this.totlaproducer = window.localStorage.getItem("producer");
    this.theTexts = "Total consumer in the area is "+this.totlaconsumer+" and they consume "+this.totalConsumeEnergy+" Kilo watt energy. Total prosumer in the area is "+ this.totlaprosumer+" and they consume "+this.totalProsumeEnergy+" Kilo watt energy. total producer in the area is " +this.totlaproducer;
       
    window.speechSynthesis.speak(new SpeechSynthesisUtterance(this.theTexts));
  }
  arrayMap(obj, fn) {
    var aray = [];
    for (var i = 0; i < obj.length; i++) {
      aray.push(fn(obj[i], i));
    }
    return aray;

  }

  mapReady(map) {
    const bounds: LatLngBounds = new google.maps.LatLngBounds();
    for (const mm of this.markers) {
     
      if (mm.lat != 0 && mm.lng != 0)
        bounds.extend(new google.maps.LatLng(mm.lat, mm.lng));


    }
    if (this.markers.length === 1) {

      map.setZoom(17)
      this.spinnerService.hide();


    }
    else {
      map.fitBounds(bounds);
      this.spinnerService.hide();

    }
  }

  ngAfterViewInit() {
  }

  














  //


  getMarkersAndLoadMap(latsnlongs) {
    this.markers = latsnlongs;
  
  }













  mapIdle(infoWindow) {
  
    // if (infoWindow.lastOpen != null) {
    //   console.log("not open")
    //   infoWindow.lastOpen.close();
    // }
  }



  clickedMarker(infoWindow) {
 
    if (infoWindow != null) {
      
      infoWindow.lastOpen.close();
    }

  infoWindow.lastOpen = infoWindow;


    infoWindow.open();
    

    // const nyc = new google.maps.LatLng(users$.lat, users$.lng);
    // const london = new google.maps.LatLng(47.381714, 8.568479);
    // const distance = google.maps.geometry.spherical.computeDistanceBetween(nyc, london);
    // const d = distance / 1000
    // const r = Math.floor(d)
    // this.distance = r + " " + "km"
    // console.log(d + " " + "Km away");


  }



  onMouseOver(infoWindow, gm) {
 

    if (gm.lastOpen != null) {
     
      gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;


    infoWindow.open();
    
  }


  onClickInfoView(users$) {
   

  }

  styleFunc(feature) {
    // get level - 0/1
    var level = feature.getProperty('level');
    var color = '#FFA07A';
    var colors = '#FF0000'
    // only show level one features
    var visibility = level == 1 ? true : false;
    return {
      // icon for point geometry(in this case - doors)
      icon: 'assets/images/geopower.png',
      // set fill color for polygon features
      fillColor: color,
      // stroke color for polygons
      strokeColor: colors,
      strokeWeight: 1,
      // make layer 1 features visible
    //  visible: visibility
    };
  }


}
