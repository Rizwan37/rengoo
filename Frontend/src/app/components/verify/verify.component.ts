import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router, ActivatedRoute} from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.css']
})
export class VerifyComponent implements OnInit {
  public token: any;

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router :Router,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    
    // this.token = this.route.snapshot.params;
    // console.log(this.token);

    this.route.params.subscribe(
      params => {
        this.token = params.token;
       
  });

    
    
    this.authService.verifyUser(this.token).subscribe(res => {
     
       this.toastr.success(res['message'], 'Success');//example
        this.router.navigate(['/login']);     
    },
     err => {
      this.toastr.error("Invalid token", "Error");//example
      this.router.navigate(['/login']);
       return false;
      });
  }

}
