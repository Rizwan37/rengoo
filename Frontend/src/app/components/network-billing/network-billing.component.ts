import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-network-billing',
  templateUrl: './network-billing.component.html',
  styleUrls: ['./network-billing.component.css']
})
export class NetworkBillingComponent implements OnInit {

  // Buyer Variables
  buyerData: { name: string, totalUnits: number, distance: number, totalCost: number, totalBill: number }[] = [];
  buyerName: string;
  buyerTotalEnergy: any = 0;
  buyerTotalCost: number = 0;
  buyerPurchase: number = 0;
  buyerTotalBill: number = 0;
  buyerDistance: any;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  //Seller Variables
  sellerData: { name: string, unitsSold: number, totalCost: number, sellerTotalSale: number }[] = [];
  sellerName: any;
  sellerPrice: number = 0;
  sellerSale: number = 0;
  sellerTotalCost: number = 0;
  totalSale: number = 0;
  sellerDistance: number = 0;

  //Grid Variables
  gridData: { buyerName: string, sellerName: string, unitsTransferred: number, transmittionCost: number, gridBill: number }[] = [];
  totalTransmittionCost: number = 0;
  totalGridBill: number = 0;
  buyersSellerName: string;


  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true
    };

    this.dataService.getAllUsers().subscribe(res => {



      this.arrayMap(res, item => {
        if (item["energyConsumption"].length > 0) {


          this.buyerName = item.name;
          this.buyerDistance = item.distanceFromGrid;

          this.arrayMap(item["energyConsumption"], energy => {

            this.buyerPurchase += energy.unitsPurchased;
            this.buyerTotalCost += energy["systemCost"] + energy["productionCost"] + energy["taxCost"] + energy["transmittionCost"];
            this.buyerTotalBill += energy["totalBill"];
            this.buyersSellerName = energy["name"];

            //Grid Bill
            this.totalTransmittionCost += energy["transmittionCost"];
            this.totalGridBill += energy["gridBill"];

          })

          this.buyerData.push({ name: this.buyerName, totalUnits: this.buyerPurchase, distance: this.buyerDistance, totalCost: this.buyerTotalCost, totalBill: this.buyerTotalBill });

          this.gridData.push({ buyerName: this.buyerName, sellerName: this.buyersSellerName, unitsTransferred: this.buyerPurchase, transmittionCost: this.totalTransmittionCost, gridBill: this.totalGridBill })
          this.buyerPurchase = 0
          this.buyerTotalCost = 0;
          this.buyerTotalBill = 0;
          this.totalTransmittionCost = 0;
          this.totalGridBill = 0;
        }   //energy consumption end



        if (item["energySold"].length > 0) {

          this.sellerName = item.name;
          this.sellerPrice = item.pricePerKWH;

          this.arrayMap(item["energySold"], sale => {
            this.sellerSale += sale.energyAmount;
            this.sellerTotalCost += sale.productionCost;
            this.totalSale += this.sellerPrice * this.sellerSale;
          })
          this.sellerData.push({ name: this.sellerName, unitsSold: this.sellerSale, totalCost: this.sellerTotalCost, sellerTotalSale: this.totalSale })
          this.sellerSale = 0
          this.sellerTotalCost = 0;
          this.totalSale = 0;

        }
        //energy sole  is end
      })
      this.dtTrigger.next();
      // here need to trigger



    })
  }

  arrayMap(obj, fn) {
    var aray = [];
    for (var i = 0; i < obj.length; i++) {
      aray.push(fn(obj[i], i));
    }
    return aray;

  }
}
