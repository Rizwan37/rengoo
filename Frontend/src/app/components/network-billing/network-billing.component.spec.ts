import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NetworkBillingComponent } from './network-billing.component';

describe('NetworkBillingComponent', () => {
  let component: NetworkBillingComponent;
  let fixture: ComponentFixture<NetworkBillingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NetworkBillingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworkBillingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
