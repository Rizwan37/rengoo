import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Subject } from 'rxjs';
import { OnDestroy } from '@angular/core';
import { b } from '@angular/core/src/render3';
import * as moment from 'moment';
declare var Highcharts: any;

@Component({
  selector: 'app-energy-production',
  templateUrl: './energy-production.component.html',
  styleUrls: ['./energy-production.component.css']
})
export class EnergyProductionComponent implements OnInit, OnDestroy {
  userId: any;
  Production = [];
  unSortedArray: any;
  sortedArray: any;
  amout = [];
  resouce = [];
  totalProduction: number = 0;
  fullDate: any;
  chart_timestamps = [];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  constructor(
    private authService: AuthService,
    private dataService: DataService,
    private spinnerService: Ng4LoadingSpinnerService,

  ) { }

  ngOnInit() {

    this.spinnerService.show();
    this.authService.getProfile().subscribe(profile => {

      this.userId = profile['user']._id;

      this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 5,
        processing: true
      };
      this.dataService.getEnergyProduction(this.userId).subscribe(res => {
        this.unSortedArray = res;
        this.sortedArray = this.unSortedArray.sort((a, b) => {
          return <any>new Date(b.timestamp) - <any>new Date(a.timestamp)
        })
        this.Production = this.arrayMap(this.sortedArray, (energy, index) => {
          // return {
          this.amout.push(energy.energyResource);
          this.resouce.push(energy.production);
          this.totalProduction += energy.production;
          this.fullDate = moment(new Date(energy.timestamp)).format("DD-MMMM-YYYY");
          this.chart_timestamps.push(moment(new Date(energy.timestamp)).format('DD-MMMM').slice(0, 6))
          return {
            eResource: energy.energyResource,
            eProduction: energy.production,
            eDate: this.fullDate
          }

        });

        // this.chart_timestamps = this.arrayMap(this.sortedArray, item => );




        this.dtTrigger.next();

        this.renderHighchart();
        this.spinnerService.hide();


      }, err => {
        this.spinnerService.hide();
      });

    });
  }



  arrayMap(obj, fn) {
    var aray = [];
    for (var i = 0; i < obj.length; i++) {
      aray.push(fn(obj[i], i));
    }
    return aray;

  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }


  //
  renderHighchart() {
    Highcharts.chart('consumption', {
      chart: {
        type: 'areaspline'
      },
      title: {
        text: 'Energy Production'
      },
      legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'top',
        x: 150,
        y: 100,
        floating: true,
        borderWidth: 1,
        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
      },
      xAxis: {
        categories: this.chart_timestamps,
        // plotBands: [{ // visualize the weekend
        //   from: 4.5,
        //   to: 6.5,
        //   color: 'rgba(68, 110, 213, .2)'
        // }]
      },
      yAxis: {
        title: {
          text: 'kWh'
        }
      },
      tooltip: {
        shared: true,
        valueSuffix: ' units'
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        areaspline: {
          fillOpacity: 0.5
        }
      },
      series: [{
        name: 'Energy Produced',
        data: this.resouce
      }]
    });
  }

  //












}
