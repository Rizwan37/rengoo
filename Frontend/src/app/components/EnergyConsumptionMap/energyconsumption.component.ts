import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { GoogleMapsAPIWrapper, AgmMap, LatLngBounds, LatLngBoundsLiteral, AgmCoreModule, MapsAPILoader } from '@agm/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { EnergyConsumptionService } from '../../services/energyconsumption.service'
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

declare const google: any;

@Component({
  selector: 'app-energyconsumption',
  templateUrl: './energyconsumption.component.html',
  styleUrls: ['./energyconsumption.component.css']
})


export class EnergyConsumptionComponent {
  @ViewChild('AgmMap') agmMap: AgmMap;
  welcomeTxt: string = `Welcome to party!`;
  lat: number = 51.678418;
  lng: number = 7.809007;
  distance: String;
  users$: Object;
  map = null;
  latitudedata = [];
  mapData = [];


  selectedOption: string;
  markers = [];
  constructor(
    private _energyconsump: EnergyConsumptionService, private spinnerService: Ng4LoadingSpinnerService

  ) { }

  ngOnInit() {

    this.spinnerService.show();

    this._energyconsump.getEnergyConsumption(JSON.parse(localStorage.getItem("user")).id).subscribe(datas => {

      if (datas['energyConsumption'] && datas['energyConsumption'].length > 0) {
        var energyConsumption = this.addEnergyAndremoveRepeatedBuyers(datas['energyConsumption']);

      }

      this.latitudedata = datas['energyConsumption'];
      //this.userType = datas["userType"]; //To be added after addition of userType for EnergyConsumption in Chaincode
      this.spinnerService.hide();
      this.getMarkersAndLoadMap(this.latitudedata);

    });
  }

  arrayMap(obj, fn) {
    var aray = [];
    for (var i = 0; i < obj.length; i++) {
      aray.push(fn(obj[i], i));
    }
    return aray;

  }

  mapReady(map) {
    const bounds: LatLngBounds = new google.maps.LatLngBounds();
    for (const mm of this.markers) {
      // if(mm.lat != 0 && mm.lng != 0)

      bounds.extend(new google.maps.LatLng(mm.latitude, mm.longitude));
    }

    if (this.markers.length === 1) {

      map.setZoom(17)
      this.spinnerService.hide();


    }
    else {
      map.fitBounds(bounds);
      this.spinnerService.hide();

    }
  }

  ngAfterViewInit() {
  }

  addEnergyAndremoveRepeatedBuyers(aray) {
    for (var i = 0; i < aray.length; i++) {
      for (var j = i + 1; j < aray.length; j++) {
        if (aray[j].seller == aray[i].seller) {
          aray[i].unitsPurchased += aray[j].unitsPurchased;
          aray.splice(aray.indexOf(aray[j]), 1);
          j--;
        }
      }
    }
    return aray;
  }

  getMarkersAndLoadMap(latsnlongs) {
    this.markers = latsnlongs;

  }

  clickedMarker(users$) {


    const nyc = new google.maps.LatLng(users$.lat, users$.lng);
    const london = new google.maps.LatLng(47.381714, 8.568479);
    const distance = google.maps.geometry.spherical.computeDistanceBetween(nyc, london);
    const d = distance / 1000;
    const r = Math.floor(d);
    this.distance = r + " " + "km";
  }

  icon = {
    url: 'http://localhost:4200/assets/img/geopower.png',

  }

  onMouseOver(infoWindow, gm) {

    if (gm.lastOpen != null) {
      gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;
    infoWindow.open();

  }

  onClickInfoView(users$) {

  }
  mapIdle() { }
}



