import { Component, OnInit } from "@angular/core";
import { AuthService } from '../../../services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.css']
})

export class UserProfileComponent implements OnInit {
    storage: any;
    ava: any;

    user: Object;
    constructor(
        private authService: AuthService,
        private toastr: ToastrService,
        private spinnerService: Ng4LoadingSpinnerService
    ) {

    }


    ngOnInit(): void {

        const abc = JSON.parse(localStorage.getItem("power"))
        if (abc == true) {
            this.storage = "ON"
        }
        else {
            this.storage = "OFF"
        }

        const asd = JSON.parse(localStorage.getItem("available"))
        if (asd == true) {
            this.ava = "ON"
        }
        else {
            this.ava = "OFF"
        }

        this.authService.getProfile().subscribe(profile => {

            this.user = profile['user'];
            this.spinnerService.hide();
        },
            err => {
                this.spinnerService.hide();
                return false;
            });
    }

}