import { Component, OnInit, ViewChild } from '@angular/core';
import { NavbarComponent } from '../navbar/navbar.component';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Observable } from "rxjs/Observable";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { UserEnergyMixComponent } from './user-energy-mix/user-energy-mix.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

declare var $: any;
declare var Highcharts: any;
@Component({
  selector: 'app-user-panel-navbar',
  templateUrl: './user-panel-navbar.component.html',
  styleUrls: ['./user-panel-navbar.component.css']
})
export class UserPanelNavbarComponent implements OnInit {

  @ViewChild(NavbarComponent) navbar: NavbarComponent;

  @ViewChild(UserEnergyMixComponent) energyMix: UserEnergyMixComponent;

  formTokenBalance: FormGroup;
  user: string;
  energies: any;
  timestamps: any;
  userBalanceAmount: any;
  totalEnergyConsumed: any;
  userId: any;
  // consumptions[]:any;
  EnergyPercentage: any;
  EnergyName: any;
  userType: any;
  powerStorage: any;




  //Energy Sold Variable
  energySoldLength: any;

  constructor(
    private authService: AuthService,
    private dataService: DataService,
    private router: Router,
    private spinnerService: Ng4LoadingSpinnerService,
    private flashMessage: FlashMessagesService,
    private formbuilder: FormBuilder,
    private toastr: ToastrService) { }

  ngOnInit() {

    this.formTokenBalance = this.formbuilder.group({
      user: ['waltson.poc.hyperledger.User#', [
      ]],
      balance: ['', [
        Validators.required,
        Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')
      ]]
    })


    this.authService.getProfile().subscribe(profile => {

      this.user = profile['user']['username'];

      this.userId = profile['user']._id;

      this.spinnerService.hide();
    },
      err => {
        this.spinnerService.hide();
        return false;
      });


    this.dataService.getMix(JSON.parse(localStorage.getItem("user")).id).subscribe(res => {
      this.energySoldLength = res["energySold"].length;
      this.userType = res["userType"];
      this.powerStorage = res["powerStorageRequirement"];


    }, err => {
      this.spinnerService.hide();
    });

    //For User Balance
    this.dataService.getMix(JSON.parse(localStorage.getItem("user")).id).subscribe(res => {

      this.userBalanceAmount = res['balanceAmount'];
      this.totalEnergyConsumed = res['totalEnergyConsumed'];
      this.spinnerService.hide();


    }, err => {
      this.spinnerService.hide();
    });


    setTimeout(
      function () {
        location.reload();
      }, 900000); //Page Refresh after 15 minutes
    
    setTimeout(() => {
      this.reCall();
    }, 5000 );
    // this.updateUserBalance();
    // this.reCall();
  }

  reCall() {
    this.dataService.getMix(JSON.parse(localStorage.getItem("user")).id).subscribe(res => {
      this.userBalanceAmount = res['balanceAmount'];
      setTimeout(()=>{
        this.reCall();
      }, 5000);
    }, err => {
      setTimeout(() => {
        this.reCall
      }, 5000);
    });
  }

  get balance() {
    return this.formTokenBalance.get('balance');
  }

  arrayMap(obj, fn) {
    var aray = [];
    for (var i = 0; i < obj.length; i++) {
      aray.push(fn(obj[i]));
    }
    return aray;
  }

  updateBalance() {
    this.spinnerService.show();
    this.formTokenBalance.value.user = this.formTokenBalance.value.user + JSON.parse(localStorage.getItem("user")).id;
    this.formTokenBalance.value.balance = parseFloat(this.formTokenBalance.value.balance);

    this.dataService.updateTokenBalance(this.formTokenBalance.value).subscribe(res => {
      this.spinnerService.hide();
      this.toastr.success("Successfully Updated", "Success");
      $('#add-balance').modal('hide');
      this.balance.setValue('');
      this.balance.setErrors(null);
      //this.router.navigate(["UserPanelNavbarComponent"]);
      this.ngOnInit();
    }, err => {
      this.spinnerService.hide();
      this.toastr.error("Something went wrong!", "Error");
    });
  }

  onLogoutClick() {
    this.authService.logout();
    this.flashMessage.show('You are logged out', {
      cssClass: 'alert-success',
      timeout: 3000
    });
    this.router.navigate(['/login']);
    return false;
  }


  ValidateFloatNumber(controls) {
    // Create a regular expression
    const Exp = /[+-]?([0-9]*[.])?[0-9]+/;
    // const Exp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    // Test email against regular expression
    if (Exp.test(controls.value)) {
      return null;
    } else {
      return { 'validateFloatNumber': true } // Return as invalid email
    }
  }

}
