import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ToastrService } from 'ngx-toastr';
declare var $:any

@Component({
  selector: 'app-forget',
  templateUrl: './forget.component.html',
  styleUrls: ['./forget.component.css']
})
export class ForgetComponent implements OnInit {

  form;
  processing:boolean = false;
  emailVerificationResposne:any;
  emailValid;


  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private flashMessage:FlashMessagesService,
    private spinnerService: Ng4LoadingSpinnerService,
    private toastr: ToastrService
  ) {
    this.createForm();
   }

  ngOnInit() {

  }




   // Function to validate e-mail is proper format
   validateEmail(controls) {
    // Create a regular expression
    const regExp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    // Test email against regular expression
    if (regExp.test(controls.value)) {
      return null; // Return as valid email
    } else {
      return { 'validateEmail': true } // Return as invalid email
    }
  }

  submitForget(){

   const email = this.form.get('email').value;
     this.spinnerService.show();
   this.authService.forgetPassword(email).subscribe(res=>{
     
     if(res['success'] === true){
        this.spinnerService.hide();
      this.toastr.success('An email has been send to your account ', "Success");//example
     // this.form.get('email').setValue('');

     this.emailVerificationResposne = res['message'];
     this.router.navigate(['/login']);

     }else{
         this.spinnerService.hide();
      this.toastr.error('Something went wrong please try again ', 'Error');//example
      this.form.get('email').setValue('');

     }
   }, err => {
     this.spinnerService.hide();
   });
  }


  // Function to check if e-mail is taken
  checkEmail() {

        let email = this.form.get('email').value;
        
        if(email == null || email == '' || email == undefined)
        {
          this.flashMessage.show('Please enter an email address', {
            cssClass:'alert-danger text-center',
            timeout: 3000
          });
          this.toastr.error("Please enter an email address ", "Error");//example
          return false;
        }
          this.spinnerService.show();
    // Function from authentication file to check if e-mail is taken
    this.authService.checkEmail(this.form.get('email').value).subscribe(data => {
      // Check if success true or false was returned from API
     

      if (data['success'] === false) {
        this.processing = true;
        this.toastr.success("Email is verified", "Success");//example
          this.emailValid = true;
        this.spinnerService.hide();
      } else {
        this.emailValid = false;
        this.toastr.error("Email does not exits ", "Error");//example
        this.form.get('email').setValue('');
        this.spinnerService.hide();
      }
    }, err => {
      this.spinnerService.hide();
    });
  
  }


    // Function to create registration form
    createForm() {
      this.form = this.formBuilder.group({
        // Email Input
        email: ['', Validators.compose([
          Validators.required, // Field is required
          this.validateEmail // Custom validation
        ])],

        }); // Add custom validator to form for matching passwords
    }

}
