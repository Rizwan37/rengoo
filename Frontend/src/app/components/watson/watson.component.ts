import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit
} from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  EnergySoldService
} from '../../services/energySold.service'
declare var google: any;

@Component({
  selector: 'app-watson',
  templateUrl: './watson.component.html',
  styleUrls: ['./watson.component.css']
})
export class WatsonComponent implements OnInit {


  title: string = 'Mapping with Angular';
  //  lat: number = 47.67713711448439;
  //  lng: number = 8.6679876976368;
  lat: number = 47.67713711448439;
  lng: number = 8.6679876976368;
   zoom: number = 16;
 
 
 
 
   
 

  
  geoJsonObject: Object;


  constructor(private _energysold: EnergySoldService) { }

  getGeoJSON(): void {

    this._energysold.getBermuda().subscribe(datas => {
      this.geoJsonObject = datas;

       console.log("dddisididd",this.geoJsonObject);
   

     
    });
  }








 


  ngOnInit() : void {
    this.getGeoJSON();
    console.log("ddddd",this.getGeoJSON);
  }
  styleFunc(feature) {
    // get level - 0/1
    var level = feature.getProperty('level');
    var color = 'red';
    // only show level one features
    var visibility = level == 1 ? true : false;
    return {
      // icon for point geometry(in this case - doors)
      icon: 'assets/images/geopower.png',
      // set fill color for polygon features
      fillColor: color,
      // stroke color for polygons
      strokeColor: color,
      strokeWeight: 1,
      // make layer 1 features visible
    //  visible: visibility
    };
  }



}