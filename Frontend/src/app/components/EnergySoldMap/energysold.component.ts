import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit
} from '@angular/core';
import {
  GoogleMapsAPIWrapper,
  AgmMap,
  LatLngBounds,
  LatLngBoundsLiteral,
  AgmCoreModule,
  MapsAPILoader
} from '@agm/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import {
  EnergySoldService
} from '../../services/energySold.service'
import {
  Observable
} from 'rxjs';
import {
  map
} from 'rxjs/operators';

declare const google: any;

@Component({
  selector: 'app-energysold',
  templateUrl: './energysold.component.html',
  styleUrls: ['./energysold.component.css']
})


export class EnergySoldComponent implements OnInit, AfterViewInit {
  @ViewChild('AgmMap') agmMap: AgmMap;
  welcomeTxt: string = `Welcome to party!`;
  lat: number = 51.678418;
  lng: number = 7.809007;
  distance: String;
  users$: Object;
  map = null;
  latitudedata = [];
  mapData = [];
  match = [];
  unmatch = [];
  selectedOption: string;
  markers = [];
  userType: any;
  constructor(
    private _energysold: EnergySoldService, private spinnerService: Ng4LoadingSpinnerService

  ) { }

  ngOnInit() {

    this.spinnerService.show();

    this._energysold.getEnergySold(JSON.parse(localStorage.getItem("user")).id).subscribe(datas => {

      if (datas['energySold'] && datas['energySold'].length > 0) {
        var energySold = this.addEnergyAndremoveRepeatedBuyers(datas['energySold']);
      }

      this.latitudedata = datas['energySold']
      //this.userType = datas["userType"];//Can be added after change Addition of User Type for EnergySold in chaincode
      this.getMarkersAndLoadMap(this.latitudedata);
      this.spinnerService.hide();
    });
  }

  arrayMap(obj, fn) {
    var aray = [];
    for (var i = 0; i < obj.length; i++) {
      aray.push(fn(obj[i], i));
    }
    return aray;

  }

  mapReady(map) {
    const bounds: LatLngBounds = new google.maps.LatLngBounds();
    for (const mm of this.markers) {
      if (mm.lat != 0 && mm.lng != 0)

        bounds.extend(new google.maps.LatLng(mm.latitude, mm.longitude));
    }
    //map.setZoom(17);

    if (this.markers.length === 1) {

      map.setZoom(17)
      this.spinnerService.hide();


    }
    else {
      map.fitBounds(bounds);
      this.spinnerService.hide();

    }
  }

  addEnergyAndremoveRepeatedBuyers(aray) {
    for (var i = 0; i < aray.length; i++) {
      for (var j = i + 1; j < aray.length; j++) {
        if (aray[j].buyer == aray[i].buyer) {
          aray[i].energyAmount += aray[j].energyAmount;
          aray.splice(aray.indexOf(aray[j]), 1);
          j--;
        }
      }
    }
    return aray;
  }


  ngAfterViewInit() { }

  getMarkersAndLoadMap(latsnlongs) {
    this.markers = latsnlongs;

  }


  mapIdle() {

  }



  clickedMarker(users$) {


    const nyc = new google.maps.LatLng(users$.lat, users$.lng);
    const london = new google.maps.LatLng(47.381714, 8.568479);
    const distance = google.maps.geometry.spherical.computeDistanceBetween(nyc, london);
    const d = distance / 1000
    const r = Math.floor(d)
    this.distance = r + " " + "km"



  }

  icon = {
    url: 'http://localhost:4200/assets/img/geopower.png',

  }


  onMouseOver(infoWindow, gm) {

    if (gm.lastOpen != null) {
      gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;
    infoWindow.open();

  }


  onClickInfoView(users$) {

  }
}
